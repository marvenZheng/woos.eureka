package ai.sparklabinc.woos;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.netflix.config.AbstractPollingScheduler;
import com.netflix.config.FixedDelayPollingScheduler;
import com.netflix.config.PollResult;
import com.netflix.config.PolledConfigurationSource;

public class DynamicConfigurationSource implements PolledConfigurationSource {
	
	private AbstractPollingScheduler scheduler = new FixedDelayPollingScheduler(2000,2000,false);
	public PollResult poll(boolean arg0, Object arg1) throws Exception {
		Map<String,Object> map = new HashMap<>();
        map.put("test",UUID.randomUUID().toString());
        return PollResult.createFull(map);
	}
}


class MyScheduler extends AbstractPollingScheduler {

	@Override
	protected void schedule(Runnable arg0) {
		
	}

	@Override
	public void stop() {
		
	}
	
}
